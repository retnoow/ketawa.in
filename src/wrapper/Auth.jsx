import { Navigate, useLocation } from "react-router-dom"
import { useAuthState } from "react-firebase-hooks/auth"
import { getAuth } from "firebase/auth"
import firebaseConfig from "../config/firebaseConfig"

const Auth = ({children}) => {

    const auth = getAuth(firebaseConfig)
    const [user, loading] = useAuthState(auth)
    let location = useLocation()
    
    if(loading)
        return <h5>checking auth... </h5>
    else if(!user && !loading) 
        return <Navigate to="/login" state={{from: location}} replace />
    return children

}

export default Auth