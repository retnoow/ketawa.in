import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter,  Routes, Route } from "react-router-dom";
import Register from './components/Register';
import Auth from './wrapper/Auth';
import Login from './components/Login';
import Jokes from './components/Jokes';
import AddJokes from './components/AddJokes';
import Detail from './components/Detail';
import { ToastContainer } from 'react-toastify';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
<React.StrictMode>
<ToastContainer />
    <BrowserRouter>
    <Routes>
        <Route path="/" element={<App />} >
        <Route path="create" element={<AddJokes />} />
        <Route path="detail/:id" element={<Detail />} />
        <Route path="register" element={<Register />} />
        <Route path="login" element={<Login />} />
        <Route
          path="/"
          element={
            <Auth>
               <div>
                  <Jokes />
                {/* <div className="col-md-4">
                  <AddJokes />
                </div> */}
              </div>
            </Auth>
          } />
      </Route>
    </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
