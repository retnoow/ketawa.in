import React from 'react'
import { useAuthState } from 'react-firebase-hooks/auth'
import {getAuth} from 'firebase/auth'
import firebaseApp from '../config/firebaseConfig'
import { getFirestore, doc, updateDoc, arrayUnion, arrayRemove} from 'firebase/firestore'
import { toast } from 'react-toastify'

const DislikeJokes = ({id, dislikes}) => {
    const auth = getAuth (firebaseApp)
    const disliked = getFirestore(firebaseApp)
    const [user] = useAuthState(auth)

    const likesRef = doc(disliked, "jokes", id)

    const DislikesHandler = () =>{
      if(dislikes?.includes(user.uid)) {
        updateDoc(likesRef, {
          dislikes: arrayRemove(user.uid),

        }). then (() => {
          console.log("unreport")
        }).catch((e) =>{
          console.log(e)
        })
      } else{
        updateDoc(likesRef, {
          dislikes: arrayUnion(user.uid)
        }). then (() => {
          console.log("report")
          toast("Thanks for reporting this, we'll review", {type: "success"})
        }).catch((e) =>{
          console.log(e)
        })
      }
    }



  return (
    <div>

        <i className={`fa fa-flag${!dislikes?.includes(user.uid) ? "-o" : ""} fa-lg pe-3`} 
        style= {{cursor:"pointer", color: dislikes?.includes(user.uid) ? "black" : null, }}
        onClick={DislikesHandler}
        />
        
       
    </div>
  )
}

export default DislikeJokes