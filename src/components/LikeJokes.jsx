import React from 'react'
import { useAuthState } from 'react-firebase-hooks/auth'
import {getAuth} from 'firebase/auth'
import firebaseApp from '../config/firebaseConfig'
import { getFirestore, doc, updateDoc, arrayUnion, arrayRemove} from 'firebase/firestore'

const LikeJokes = ({id, likes}) => {
    const auth = getAuth (firebaseApp)
    const liked = getFirestore(firebaseApp)
    const [user] = useAuthState(auth)

    const likesRef = doc(liked, "jokes", id)

    const likesHandler = () =>{
      if(likes?.includes(user.uid)) {
        updateDoc(likesRef, {
          likes: arrayRemove(user.uid)
        }).then (() => {
          console.log("unliked")
        }).catch((e) =>{
          console.log(e)
        })
      } else{
        updateDoc(likesRef, {
          likes: arrayUnion(user.uid)
        }).then (() => {
          console.log("like")
        }).catch((e) =>{
          console.log(e)
        })
      }
    }



  return (
    <div>
        <i className={`fa fa-heart${!likes?.includes(user.uid) ? "-o" : ""} fa-lg pe-3`} 
        style= {{cursor:"pointer", color: likes?.includes(user.uid) ? "red" : null, }} 
        onClick={likesHandler}
        />
       
    </div>
  )
}

export default LikeJokes