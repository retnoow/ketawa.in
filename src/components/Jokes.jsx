import React, {useState, useEffect} from 'react'
import { useAuthState } from 'react-firebase-hooks/auth'
import {collection, getFirestore, onSnapshot, orderBy, query} from 'firebase/firestore'
import firebaseApp from '../config/firebaseConfig'
import DeleteJokes from './DeleteJokes'
import {getAuth} from 'firebase/auth'
import LikeJokes from './LikeJokes'
import DislikeJokes from './DislikeJokes'
import Slider from 'react-slick'
import { Link } from 'react-router-dom'


const Jokes= () => {
    const auth = getAuth(firebaseApp)
    const [jokes, setJokes] = useState([])
    const [user] = useAuthState(auth);

    useEffect(() => {
        const value = collection(getFirestore(firebaseApp), 'jokes')

        const q = query(value, orderBy("createdAt", "desc"));
        onSnapshot(q, (snapshot) => {
            const jokes = snapshot.docs.map((doc) => ({
                id: doc.id,
                ...doc.data(),
            }))
            setJokes(jokes)
            console.log(jokes)
        })
    },[]);

    const settings = {
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 5000,
    };
    
  return (
    <div className='slider'>
        {jokes.length === 0 ? (
                <p>No articles found</p>
            ): <Slider {...settings}> 
                {(jokes.map(({id, title, description, imageUrl, createdAt, createdBy, userId, likes, dislikes, comments}) => (
                    <div className='p-3 bg-light' key={id}>
                        <div className='d-flex flex-row-reverse'>
                                <DeleteJokes  id={id} imageUrl={imageUrl}/>
                        </div >
                        <div className='row'>
                            <div className='col-3'>
                            <Link to={`/detail/${id}`}>
                                <img src={imageUrl} alt='title' style={{height:180, width:180}} /> 
                            </Link>
                            </div>
                            <div className='col-9 ps-3'>
                                <div className='row'>
                                    <div className='col-6'>
                                        {createdBy && (
                                            <p className='badge bg-warning'>Hey i am {createdBy.toUpperCase()}</p>
                                        )}
                                    </div>

                                    <div className='col-6 d-flex flex-row-reverse'>
                                        {user && user.uid === userId && (
                                            <DeleteJokes id={id} imageUrl={imageUrl} />
                                        )}
                                    </div> 
                                </div>
                                <h2>{title}</h2>
                                <p>{createdAt.toDate().toLocaleTimeString('en-GB').substring(0, 5)} &bull; {createdAt.toDate().toDateString()}</p>
                                <h4>{description}</h4>
                                <div className='d-flex flex-row-reverse'>
                                    
                                    <div className='pe-3'> 
                                        <p>{likes?.length}  Likes </p>
                                    </div>
                                    {user && <LikeJokes id={id} likes={likes} />}

                                    <div className='pe-3'> 
                                        <p>Report</p>
                                    </div> 
                                    {user && <DislikeJokes id={id} dislikes={dislikes} />}
                                    
                                    
                                    {comments && comments.length > 0 && (
                                    <div className='pe-3'>
                                        <p><i className='fa fa-comment' /> {comments?.length} Comment</p>
                                    </div>
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                 ))
                )}
               </Slider>  
        }
    </div>
  )
}

export default Jokes