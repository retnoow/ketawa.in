import {React, useEffect, useState} from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import {useCreateUserWithEmailAndPassword } from 'react-firebase-hooks/auth'
import { updateProfile } from 'firebase/auth'
import {getAuth} from 'firebase/auth'
import firebaseApp from '../config/firebaseConfig'
import { useNavigate } from 'react-router-dom'


const auth = getAuth(firebaseApp)

const Register = () => {
    const navigate =  useNavigate()
    const [credentials, setCredentials] = useState({

        email: '',
        password: '',
        name:''
    })

const [createUserWithEmailAndPassword, user] = useCreateUserWithEmailAndPassword(auth)

useEffect(() => {
    if(user !== undefined)
        navigate('/', {replace: true})
        updateProfile(auth.currentUser, { displayName: credentials.name });
}, [user, navigate, credentials])



  return (
    <div className="mt-5">
        <Row>
            <Col xs={12} md={{span: 6, offset: 3}} className="shadow border p-4 rounded">
                <Form>
                    <h3 className="text-center">Sign Up</h3>
                    <h6 className="text-center">Sudah punya akun? <Link className='text-decoration-none' to='/login'>Masuk</Link> </h6>
                    <Form.Group className="mb-3 pt-4" controlId="formBasicEmail">
                    <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter your name" onChange={(e) => setCredentials({...credentials, name: e.target.value})} value={credentials.name} />

                        <Form.Label className="mt-3" >Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter your email" onChange={(e) => setCredentials({...credentials, email: e.target.value})} value={credentials.email} />
                        <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" onChange={(e) => setCredentials({...credentials, password: e.target.value})} value={credentials.password} />
                    </Form.Group>
                    <Button variant="primary" type="button" onClick={() => createUserWithEmailAndPassword(credentials.email, credentials.password)}>
                        Register
                    </Button>
                </Form>
            </Col>
        </Row>

       
    </div>
  )
}

export default Register