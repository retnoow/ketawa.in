import { deleteDoc, doc, getFirestore } from 'firebase/firestore'
import { deleteObject, getStorage, ref } from 'firebase/storage'
import firebaseApp from '../config/firebaseConfig'
import React from 'react'
import { toast } from 'react-toastify'
import {Dropdown } from 'react-bootstrap'

const DeleteJokes = ({id, imageUrl}) => {

    const storage = getStorage(firebaseApp)
    const value = getFirestore(firebaseApp)

    const deleteHandler = async() => {
        try{
            await deleteDoc(doc(value, "jokes", id))
            toast("Jokes successfully deleted", {type: "success"})
            const storageRef = ref(storage, imageUrl)
            await deleteObject(storageRef)
        } catch{
            toast("Failed to delete", {type:"error"})
        }
    }
    
  return (
    <div>
        <Dropdown onClick={(e) => e.stopPropagation()}>
        <Dropdown.Toggle
          id={"options-button"}
          variant="borderless-dark"
          bsPrefix="no-chevron"
          size="sm"
        >
          ...
        </Dropdown.Toggle>
        <Dropdown.Menu style={{ willChange: "transform" }}>
          <Dropdown.Item onClick={deleteHandler}>
            <span>Delete</span>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  )
}

export default DeleteJokes