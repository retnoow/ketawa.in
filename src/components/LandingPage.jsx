import React, {useEffect, useState} from 'react'
import '../style.css'
import Slider from 'react-slick'
import {Button,  Card, FormControl, InputGroup} from 'react-bootstrap'
import { useAuthState } from "react-firebase-hooks/auth"
import {getAuth} from 'firebase/auth'
import {getFirestore, collection} from 'firebase/firestore'
import {useCollection} from 'react-firebase-hooks/firestore'
import firebaseApp from '../config/firebaseConfig'

const LandingPage = () => {
    const auth = getAuth(firebaseApp)
    
    const [value] = useCollection(
      collection(getFirestore(firebaseApp), 'user'),
      {
        snapshotListenOptions: { includeMetadataChanges: true },
      }
    );

    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      speed: 2000,
      autoplaySpeed: 5000,
    };

  return (
    <div>
      <div class="container mt-4">
        <div class="row height d-flex justify-content-center align-items-center">
          <div class="col-md-8">
            <div class="search">
                <InputGroup>
                  <FormControl
                    placeholder="Type to search"
                    aria-label="Search"
                    aria-describedby="basic-addon"
                  />
                  <div className="px-4">
                    <Button variant="primary" id="button-addon">Filter by</Button>
                    <Button className="ms-1" variant="primary" id="button-addon">Sort by</Button>
                  </div>     
                </InputGroup>
            </div>              
          </div>
        </div>
      </div>
      
      <div className='slider'>
          <Slider {...settings}>
            {[1,2,3,4,5].map((item, index) => {
                return(
                  <div key={index}>
                    <div className='content'>
                      <Card className='mb-5'>
                        <Card.Img variant="top" src="" />
                        <Card.Body>
                          <Card.Text>
                            Some quick example text to build on the card title and make up the
                            bulk of the card's content.
                          </Card.Text>
                        </Card.Body>
                      </Card>
                    </div>
                  </div>
                )
            })}
          </Slider> 
      </div>


      <Card style={{ width: '40rem' }}>
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Card.Link href="#">Card Link</Card.Link>
        <Card.Link href="#">Another Link</Card.Link>
      </Card.Body>
    </Card>

    
    </div>


  )
}

export default LandingPage