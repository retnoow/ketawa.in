import React, {useState} from 'react'
import {addDoc, collection, getFirestore} from 'firebase/firestore'
import firebaseApp from '../config/firebaseConfig'
import {getDownloadURL, getStorage, ref, uploadBytesResumable} from 'firebase/storage'
import {getAuth} from 'firebase/auth'
import { Timestamp } from 'firebase/firestore'
import { toast } from 'react-toastify'
import { useAuthState } from 'react-firebase-hooks/auth'
import {useNavigate } from 'react-router-dom'
import firebaseConfig from '../config/firebaseConfig'
import { Form } from 'react-bootstrap'
import { Button, Row, Col } from 'react-bootstrap'

export default function AddJokes() {

  const navigate = useNavigate()
  const storage = getStorage(firebaseApp)
  const auth = getAuth(firebaseApp)

  const [user] = useAuthState(auth)
  const [formData, setFormData] = useState({
    title: '',
    description: '',
    imageUrl: '',
    createdAt: '',
    createdBy: '',
    userID: '',
    likes:[]

 })

  const [progress, setProgress] = useState(false)

  const handleSubmit = async () => {
    if(!formData.title || !formData.description || !formData.image){
        alert('please fill all the fields')
        return
    }
 
    const storageRef = ref(storage, `/images/${Date.now()}${formData.image.name}`)
    const uploadImg = uploadBytesResumable(storageRef, formData.image)
    
    uploadImg.on("state_changed", (snapshot) => {
        const progressBar = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
        setProgress(progressBar)
      }, (err) => {
        console.log(err)
      }, () => {
        setFormData({
            title: "",
            description: "",
            image: ""
        })
        getDownloadURL(uploadImg.snapshot.ref)
        .then((url) => {
            const db = getFirestore(firebaseConfig)
            const articleRef = collection(db, 'jokes')
            addDoc(articleRef, {
                title: formData.title,
                description: formData.description,
                imageUrl: url,
                createdAt: Timestamp.now().toDate(),
                createdBy: user.displayName,
                userID: user.uid,
                likes:[],
                dislikes:[]

            }).then(() => {
                toast("Yeay, your Jokes is posted", {type: "success"})
                setProgress(0)
                navigate( '/', {replace: true})
            })
            .catch(() => {
                toast("Sorry, failed to add", {type: "error"})
            })
        })
        
    })
  }


  return (
    <div>
        <Row className='mt-5'>
          <Col xs={8} md={{span: 6, offset: 3}}  className="shadow border p-4 rounded d-flex justify-content-center">
            <Form > 
              <h3 className='text-center mb-4'>Create Your Jokes Here</h3> 
              <Form.Label>Title</Form.Label>
              <Form.Control className='mb-3' type="text" name="title" value={formData.title} onChange={(e) => setFormData({...formData, title: e.target.value})}></Form.Control>

              <Form.Label>Description</Form.Label>
              <Form.Control  as="textarea" name="description" value={formData.description} onChange={(e) => setFormData({...formData, description: e.target.value})}></Form.Control>
           
              <Form.Label className='mt-3'>Image</Form.Label>
              <Form.Control type="file" name="image" accept='image/*' onChange={(e) => setFormData({...formData, image: e.target.files[0]})}></Form.Control>

              {progress === 0 ? null : (
                  <div className='progress mt-2'>
                    <div className='progress-bar progress-bar-stripped mt-2' style={{width: `${progress} %`}}>
                      {`uploading image ${progress} %`}
                    </div>
                  </div>
              )}
              
              <Button className='mt-3' disabled={progress} variant="warning" onClick={handleSubmit} >
                Post Jokes
              </Button>
            </Form>
          </Col>
        </Row>
    </div>
  )
}
