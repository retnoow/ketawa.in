import {useState} from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { signInWithEmailAndPassword } from 'firebase/auth'
import {getAuth} from 'firebase/auth'
import firebaseApp from '../config/firebaseConfig'
import GoogleButton from 'react-google-button'
import {useNavigate } from 'react-router-dom'


const auth = getAuth(firebaseApp)

const Login = () => {
    const navigate =  useNavigate()
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");


    const loginHandler = async () => {
        try {
          await signInWithEmailAndPassword(auth, email, password);
          navigate("/");
        } catch (error) {
          console.log(error) 
        }
    };


  return (
    <div className="mt-5">
        <Row>   
            <Col xs={12} md={{span: 6, offset: 3}} className="shadow border p-4 rounded">
                <Form>
                    <h3 className="text-center">Welcome back!</h3>
                    <h6 className="text-center">Please Login</h6>
                    <Form.Group className="mb-3 pt-4" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" onChange={(e) => {setEmail(e.target.value)}} />
                        <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" onChange={(e) => {setPassword(e.target.value)}} />
                    </Form.Group>   
                    <Button variant="primary" type="button" onClick={loginHandler}>Login
                    </Button>
                    <h6 className="text-center pt-3">Belum punya akun? <Link className='text-decoration-none' to='/register'>Buat Akun</Link> </h6>
                    <p className='line'>atau masuk dengan</p>

                    <div className='mt-4 mb-2 d-flex justify-content-center'>
                        <GoogleButton  />
                    </div>
                </Form>
            </Col>
        </Row>


        
    </div>
  )
}

export default Login