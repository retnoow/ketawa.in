import { arrayRemove, arrayUnion, onSnapshot, updateDoc, getFirestore, doc } from 'firebase/firestore'
import React, {useEffect, useState} from 'react'
import { useAuthState } from 'react-firebase-hooks/auth';
import firebaseApp from '../config/firebaseConfig'
import {getAuth} from 'firebase/auth'
import { Form } from 'react-bootstrap';
import { v4 as uuidv4 } from "uuid";


const Comment = ({id}) => {
    const auth = getAuth(firebaseApp)
    const getDB = getFirestore(firebaseApp)
    const [comment, setComment] = useState("")
    const [comments, setComments] = useState([]);
    const [currentUser] = useAuthState(auth)
    
    const commentRef = doc(getDB, "jokes", id)

    useEffect(() => {
      
      const com = doc(getDB, "jokes", id)
        onSnapshot(com, (snapshot) => {
            setComments(snapshot.data().comments)
        })
    }, [])
   
    //delete com
    const deleteHandler = (comment) => {
        console.log(comment)
        updateDoc(commentRef, {
            comments: arrayRemove(comment)
        }).then ((e) => {
                console.log(e)
        }).catch((error) =>{
                console.log(error)
        })
    }

    const postHandler = (e) => {
        if(e.key === "Enter") {
            updateDoc(commentRef, {
                comments: arrayUnion({
                    user: currentUser.uid,
                    username: currentUser.displayName,
                    comment: comment,
                    createdAt: new Date(),
                    commentId: uuidv4()
                }),
            }).then(() => {
                setComment("")
            })
        }
    }
  return (
    <div>
        Comment
        <div className='container'>
            {comments !== null &&
                comments.map(({commentId, user, comment, username, createdAt}) => (
                    <div key={commentId}>
                        <div className='border p-2 mt-2 row'> 
                            <div className='col-11'>
                                <span className={`badge ${user === currentUser.uid ? "bg-success": "bg-primary" }`}>{username}</span>
                                <span className='px-3'>{comment}</span>
                            </div>
                            <div className='col-1'>
                                {user === currentUser.uid && (
                                    <i className='fa fa-times' style={{cursor: "pointer"}} onClick={() => deleteHandler({commentId, user, comment, username, createdAt})}></i>
                                 )}
                            </div>
                        </div>
                    </div>

                ))}

            {currentUser && (
                
                <Form.Control 
                    type="text" 
                    className='form-control mt-4 mb-5' 
                    value={comment} 
                    onChange={(e) => {setComment(e.target.value)}} 
                    placeholder="Comment here" 
                    onKeyUp={(e) => {postHandler(e) }} 
                />
            )}

        </div>
    </div>
  )
}

export default Comment