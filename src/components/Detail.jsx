import { onSnapshot, getFirestore, doc } from 'firebase/firestore'
import React, {useEffect, useState} from 'react'
import { useParams } from 'react-router-dom'
import { useAuthState } from 'react-firebase-hooks/auth'
import {getAuth} from 'firebase/auth'
import LikeJokes from './LikeJokes'
import firebaseApp from '../config/firebaseConfig'
import Comment from './Comment'


const Detail = () => {
    const auth = getAuth(firebaseApp)
    const {id} = useParams()
    const [detail, setDetail] = useState(null)
    const [user] = useAuthState(auth)
    
    useEffect(() => {
        const getDB = getFirestore(firebaseApp)
        const view = doc(getDB, "jokes", id)
        onSnapshot(view, (snapshot) => {
            setDetail({ ...snapshot.data(), id: snapshot.id})
        })
    }, [])
    

  return (
    <div className="container border bg-light" style={{marginTop: 70}}>
        {detail && (
                <div className='row'>
                    <div className='col-3'>
                        <img src={detail.imageUrl} 
                        alt={detail.title} style={{width: "100%", padding: 10}} />
                    </div>
                    <div className='col-9 mt-3'>
                        <h4 className='badge bg-warning'>Author: {detail.createdBy}</h4>
                        <h2>{detail.title}</h2>
                        <h4 className='mt-5'>{detail.description}</h4>

                        <div className='mt-5'>
                            {detail.createdAt.toDate().toDateString()}
                        </div>

                        <div className='d-flex flex-row-reverse'>
                            {user && <LikeJokes id={id} likes={detail.likes} />}
                            <div className='pe-2'>
                                <p>{detail.likes.length}</p>
                            </div>
                        </div> 
                        
                        <Comment id={detail.id} />
                    </div>
                </div>
            )
        }
        
    </div>
  )
}

export default Detail