/* eslint-disable no-mixed-operators */
import 'bootstrap/dist/css/bootstrap.min.css';
import 'slick-carousel/slick/slick.css'; 
import 'slick-carousel/slick/slick-theme.css';
import 'react-toastify/dist/ReactToastify.css';
import './style.css';
import {Link, Outlet} from "react-router-dom";
import {Navbar, Nav, Container} from 'react-bootstrap'
import { useAuthState } from "react-firebase-hooks/auth"
import { getAuth, signOut } from "firebase/auth"
import firebaseConfig from "./config/firebaseConfig"
import 'font-awesome/css/font-awesome.min.css';

function App() {

  const auth = getAuth(firebaseConfig)
  const [user] = useAuthState(auth)

  
  return (
    <div className="App">
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#home">KETAWA.IN</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              {user && <Nav.Link as={Link} to="/create">Create Jokes</Nav.Link>}
            </Nav>
            <Nav className="ms-auto">
              
              {!user && <Nav.Link as={Link} to="/register">Register</Nav.Link>}
              {!user && <Nav.Link as={Link} to="/login">Login</Nav.Link>}

              {user && (
              <div className='d-flex align-items-center'>
                <p className='text-dark mb-0'>Signed as, {user.displayName} </p>
                
                <div className='px-2'>
                  <Nav.Link onClick={() => signOut(auth)}>Logout</Nav.Link>
                </div>
              </div>  )}

            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <div className="container">
        <Outlet />
      </div>
    </div>
  );
}

export default App;
